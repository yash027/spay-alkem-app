import { GlobalService } from "../../../services/global.service";
import { Component, OnInit } from "@angular/core";
import { ChangePasswordService } from "src/app/services/user-account/change-password.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.page.html",
  styleUrls: ["./change-password.page.scss"]
})
export class ChangePasswordPage implements OnInit {
  showIcon = false;
  constructor(
    private service: GlobalService,
    private httpService: ChangePasswordService,
    private router: Router
  ) {}

  ngOnInit() {}
  sendpassword(formdata) {
    if (formdata.password === "" || formdata.repassword === "") {
      this.service.displayToast("password can not be empty", "danger");
    } else {
      if (formdata.password === formdata.repassword) {
        this.httpService
          .changePassword(
            "/account/change_password?password=" + formdata.password + "&user=",
            formdata.password
          )
          .subscribe(
            response => {
              this.service.displayToast(
                "Password changed successfully.",
                "success"
              );
              this.router.navigate(["/menu/inbox"]);
            },
            error => {
              this.service.displayToast(
                "Unable to change the password",
                "danger"
              );
            }
          );
      } else {
        this.service.displayToast("Password does not match");
      }
    }
  }

  showPassword() {
    this.showIcon = !this.showIcon;
  }
}
