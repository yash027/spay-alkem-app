import { PopoverController, AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"]
})
export class ProfilePage implements OnInit {
  constructor(
    private router: Router,
    private popOver: PopoverController,
    private alert: AlertController
  ) {}

  ngOnInit() {}

  logout() {
    this.popOver.dismiss();
    this.alert
      .create({
        header: "Logout",
        message: "You Really want to logout.",
        buttons: [
          {
            text: "OK",
            cssClass: "danger",
            handler: item => {
              localStorage.clear();
              this.router.navigate(["/login"]);
            }
          },
          {
            text: "cancel",
            role: "cancel",
            handler: () => {
              // this.alert.dismiss();
            }
          }
        ]
      })
      .then(alert => {
        alert.present();
      });
  }

  changePassword() {
    this.popOver.dismiss();
    this.router.navigate(["/profile/change-password"]);
  }

  closePopup() {
    this.popOver.dismiss();
    this.router.navigate(["/user-profile"]);
  }
}
