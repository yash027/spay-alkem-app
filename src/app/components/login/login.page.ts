import { GlobalService } from "./../../services/global.service";
import { ForgetPasswordPage } from "./../forget-password/forget-password.page";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { authService } from "src/app/services/auth/auth.service";
import { ModalController, LoadingController, Platform } from "@ionic/angular";
import { StatusBar } from "@ionic-native/status-bar/ngx";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  // @ViewChild("dynamicLogin", { read: ViewContainerRef, static: false })
  // dynamicLogin: ViewContainerRef;

  showIcon = false;
  backbutton = 0;
  subscribe;

  ngOnInit() {}

  constructor(
    private loginService: authService,
    private loadingController: LoadingController,
    private router: Router,
    private modalCtrl: ModalController,
    private platform: Platform,
    private statusBar: StatusBar,
    private service: GlobalService // private http: HttpClient, // private resolver: ComponentFactoryResolver, // private compiler: Compiler, // private sanitizer: DomSanitizer
  ) {}

  ionViewWillEnter() {
    this.initializeApp();
  }

  onLogin(form) {
    this.loadingController
      .create({
        message: "Loading..."
      })
      .then(loader => {
        loader.present();
      });
    form.value.rememberMe = true;
    this.loginService.loginUser("/authenticate", form.value).subscribe(
      response => {
        let token = response["id_token"];
        localStorage.setItem("token", token);
        this.loadingController.dismiss();
        this.router.navigate(["/menu/inbox"]);
        form.reset();
      },
      error => {
        if (error.status == 401 || error.status == 400) {
          this.service.displayToast("Wrong Credentials", "danger");
        } else if (error.status == 522) {
          this.service.displayToast(
            "Connection time out. Please try after sometime",
            "danger"
          );
        } else
          this.service.displayToast(
            "Error while logging in. Please try after sometime",
            "danger"
          );
        this.loadingController.dismiss();
        form.reset();
      }
    );
  }

  showPassword() {
    this.showIcon = !this.showIcon;
  }

  passwordChangeRequest() {
    this.router.navigate(["/forget-password"]);
  }

  onKeyPress(keyCode, form) {
    if (keyCode === 13) {
      if (form.value) {
        this.onLogin(form);
      }
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.subscribe = this.platform.backButton.subscribeWithPriority(
        666666,
        () => {
          if (this.router.url == "/login") {
            if (this.backbutton == 0) {
              this.backbutton++;
              this.service.displayToast("press again to exit.", "dark");
              setTimeout(() => {
                this.backbutton = 0;
              }, 3000);
            } else navigator["app"].exitApp();
          }
        }
      );
      this.statusBar.styleDefault();
    });
  }
}
//   this.getData();
// }
// ionViewDidEnter() {}

// getData() {
//   this.http
//     .get("assets/form.html", { responseType: "text" })
//     .subscribe(response => {
//       this.template = this.sanitizer.bypassSecurityTrustHtml(response);
//       this.addComponent();
//     });
// }
// private addComponent() {
//   let template = this.template.changingThisBreaksApplicationSecurity;

//   @Component({
//     template: template
//   })
//   class DynamicComponent {
//     constructor(public _parent: LoginPage) {}
//   }

//   @NgModule({
//     imports: [FormsModule, BrowserModule, CommonModule, IonicModule],
//     declarations: [DynamicComponent]
//   })
//   class DynamicComponentModule {}

//   const mod = this.compiler.compileModuleAndAllComponentsSync(
//     DynamicComponentModule
//   );
//   const factory = mod.componentFactories.find(
//     comp => comp.componentType === DynamicComponent
//   );
//   const component = this.dynamicLogin.createComponent(factory);
// }

// onClick(formdata) {
//   console.log(formdata);
//   console.log(this.fakedata);
