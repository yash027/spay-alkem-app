import { authGuard } from "./../guard/auth.guard";
import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "menu/inbox",
    pathMatch: "full",
    canActivate: [authGuard]
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then(m => m.LoginPageModule)
  },
  {
    path: "menu",
    loadChildren: () =>
      import("./menu/menu.module").then(m => m.MenuPageModule),
    canActivate: [authGuard]
  },
  {
    path: "forget-password",
    loadChildren: () =>
      import("./forget-password/forget-password.module").then(
        m => m.ForgetPasswordPageModule
      )
  },
  {
    path: "profile",
    loadChildren: () =>
      import("./profile-popover/profile.module").then(m => m.ProfilePageModule),
    canActivate: [authGuard]
  },
  {
    path: "user-profile",
    loadChildren: () =>
      import("./user-profile/user-profile.module").then(
        m => m.UserProfilePageModule
      ),
    canActivate: [authGuard]
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
