import { authGuard } from "./../../guard/auth.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { UserProfilePage } from "./user-profile.page";

const routes: Routes = [
  {
    path: "",
    component: UserProfilePage,
    canActivate: [authGuard]
  },
  {
    path: "settings",
    loadChildren: () =>
      import("./settings/settings.module").then(m => m.SettingsPageModule),
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfilePageRoutingModule {}
