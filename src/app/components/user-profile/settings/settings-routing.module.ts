import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SettingsPage } from "./settings.page";
import { authGuard } from "src/app/guard/auth.guard";

const routes: Routes = [
  {
    path: "",
    component: SettingsPage,
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsPageRoutingModule {}
