import { SubstitutionService } from "./../../../services/user-account/substitution.service";
import { GlobalService } from "./../../../services/global.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"]
})
export class SettingsPage implements OnInit {
  userInfo: any;
  segment = "about";
  substitute: any;
  constructor(
    private service: GlobalService,
    private substituteService: SubstitutionService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.service.userData.subscribe(
      info => {
        this.userInfo = info;
      },
      error => {
        this.service.displayToast("No Data Found", "danger");
      }
    );
    this.substituteService.substituteList("/substitutions").subscribe(
      response => {
        this.substitute = response;
        console.log(this.substitute);
      },
      error => {
        this.service.displayToast("Some error Occured", "danger");
      }
    );
  }
  segmentChanged(event) {
    this.segment = event.detail.value;
  }
}
