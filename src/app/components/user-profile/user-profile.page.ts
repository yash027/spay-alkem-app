import { GlobalService } from "./../../services/global.service";
import { UserAccountService } from "./../../services/user-account/user-account.service";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.page.html",
  styleUrls: ["./user-profile.page.scss"]
})
export class UserProfilePage implements OnInit {
  accountInfo: any = {};
  loading = false;

  constructor(
    private router: Router,
    private service: UserAccountService,
    private globalService: GlobalService,
    private alert: AlertController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.loading = true;
    this.service.accountInfo("/account").subscribe(
      data => {
        this.accountInfo = data;
        this.globalService.userData.next(this.accountInfo);
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.globalService.displayToast(
          "Unable to fetch account details",
          "danger"
        );
      }
    );
  }

  openContact() {
    this.alert
      .create({
        header: "HEAD OFFICE",
        subHeader: "Alkem Laboratories Limited",
        message: `Devashish Building, Alkem House,
                  Senapati Bapat Road, Lower Parel,
                  Mumbai - 400 013.
                  E-mail: contact@alkem.com
                  Tel: +91 22 3982 99 99`,
        buttons: ["Done"]
      })
      .then(alert => {
        alert.present();
      });
  }

  onLogout() {
    this.alert
      .create({
        header: "Logout !",
        message: "you really want to logout.",
        buttons: [
          {
            text: "OK",
            handler: () => {
              localStorage.clear();
              this.router.navigate(["login"]);
            }
          },
          {
            text: "cancel",
            role: "cancel",
            handler: () => {}
          }
        ]
      })
      .then(alert => {
        alert.present();
      });
  }
}
