import { GlobalService } from "./../../services/global.service";
import { forgetUserService } from "./../../services/auth/forget-user.service";

import { Component, OnInit } from "@angular/core";
import { LoadingController } from "@ionic/angular";

@Component({
  selector: "app-forget-password",
  templateUrl: "./forget-password.page.html",
  styleUrls: ["./forget-password.page.scss"]
})
export class ForgetPasswordPage implements OnInit {
  constructor(
    private service: forgetUserService,
    private globalService: GlobalService,
    private loader: LoadingController,
  ) {}

  ngOnInit() {}

  sendEmail(email) {
    this.loader
      .create({
        message: "Please wait...",
        translucent: true
      })
      .then(load => {
        load.present();
      });
    this.service
      .forgetUserPassword("/account/reset_password/init", email)
      .subscribe(
        data => {
          this.loader.dismiss();
        },
        error => {
          this.loader.dismiss();
          this.globalService.displayToast("Email does not exist", "danger");
        }
      );
  }
}
