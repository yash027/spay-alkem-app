import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportDetailNPOPageRoutingModule } from './report-detail-npo-routing.module';

import { ReportDetailNPOPage } from './report-detail-npo.page';
import { MaterialModule } from 'src/app/components/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    ReportDetailNPOPageRoutingModule
  ],
  declarations: [ReportDetailNPOPage]
})
export class ReportDetailNPOPageModule {}
