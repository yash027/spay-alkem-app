import { authGuard } from "./../../../../guard/auth.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ReportDetailNPOPage } from "./report-detail-npo.page";

const routes: Routes = [
  {
    path: "",
    component: ReportDetailNPOPage,
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportDetailNPOPageRoutingModule {}
