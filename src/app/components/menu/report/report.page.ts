import { ReportDetailNPOPage } from "./report-detail-npo/report-detail-npo.page";
import { ReportDetailPage } from "./report-detail/report-detail.page";

import { PopoverController, ModalController, Platform } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";
import { ProfilePage } from "../../profile-popover/profile.page";
import { ActivatedRoute, Router } from "@angular/router";
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: "app-report",
  templateUrl: "./report.page.html",
  styleUrls: ["./report.page.scss"]
})
export class ReportPage implements OnInit {
  reportData: any;
  loading = false;
  subscribe;
  backbutton = 0;

  constructor(
    private popoverController: PopoverController,
    private modalController: ModalController,
    private route: ActivatedRoute,
    private router: Router,
    private platform: Platform,
    private service: GlobalService
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.initializeApp();
    this.loading = false;
    this.reportData = this.route.snapshot.data["reportData"];
  }
  async profilePopover(ev) {
    const popover = await this.popoverController.create({
      component: ProfilePage,
      event: ev
    });
    return await popover.present();
  }
  async openReportDetail(index, type) {
    this.loading = true;
    if (type == "PAYABLES_PO") {
      const modal = await this.modalController.create({
        component: ReportDetailPage,
        componentProps: {
          data: this.reportData[index]
        }
      });
      this.loading = false;
      return await modal.present();
    } else {
      this.loading = true;
      const modal = await this.modalController.create({
        component: ReportDetailNPOPage,
        componentProps: {
          data: this.reportData[index]
        }
      });
      this.loading = false;
      return await modal.present();
    }
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.subscribe = this.platform.backButton.subscribeWithPriority(
        666666,
        () => {
          if (this.router.url == "/menu/report") {
            if (this.backbutton == 0) {
              this.backbutton++;
              this.service.displayToast("press again to exit.");
              setTimeout(() => {
                this.backbutton = 0;
              }, 3000);
            } else navigator["app"].exitApp();
          }
        }
      );
      //this.statusBar.styleDefault();
    });
  }
}
