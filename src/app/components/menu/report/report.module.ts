import { ReportDetailPageModule } from './report-detail/report-detail.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportPageRoutingModule } from './report-routing.module';

import { ReportPage } from './report.page';
import { ReportDetailNPOPageModule } from './report-detail-npo/report-detail-npo.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportPageRoutingModule,
    ReportDetailPageModule,
    ReportDetailNPOPageModule
  ],
  declarations: [ReportPage]
})
export class ReportPageModule {}
