import { GlobalService } from "../../../../services/global.service";
import { Component, OnInit } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";

@Component({
  selector: "app-report-detail",
  templateUrl: "./report-detail.page.html",
  styleUrls: ["./report-detail.page.scss"]
})
export class ReportDetailPage implements OnInit {
  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
    public dateService: GlobalService
  ) {}

  segmentChange = "info";
  header: any = {};
  selected;
  attachments = [];
  data: any;

  ionViewWillEnter() {
    this.data = this.navParams.get("data");
  }
  ngOnInit() {}

  segmentChanged(event) {
    this.segmentChange = event.detail.value;
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  downloadFile(url, name) {
    const link = document.createElement("a");
    link.setAttribute("target", "_blank");
    link.setAttribute("href", url);
    link.setAttribute("download", name);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
}
