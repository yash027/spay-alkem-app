import { authGuard } from "./../../../../guard/auth.guard";
import { ReportDetailService } from "./../../../../services/menu-services/submenu-services/report-detail.service";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ReportDetailPage } from "./report-detail.page";

const routes: Routes = [
  {
    path: "",
    component: ReportDetailPage,
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportDetailPageRoutingModule {}
