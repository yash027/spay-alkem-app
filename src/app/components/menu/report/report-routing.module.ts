import { authGuard } from "./../../../guard/auth.guard";
import { MenuReportService } from "./../../../services/menu-services/menu-report.service";
import { ReportDetailPage } from "./report-detail/report-detail.page";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ReportPage } from "./report.page";

const routes: Routes = [
  {
    path: "",
    component: ReportPage,
    resolve: { reportData: MenuReportService },
    canActivate: [authGuard]
  },
  {
    path: "**",
    redirectTo: "menu/report",
    pathMatch: "full",
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [ReportDetailPage]
})
export class ReportPageRoutingModule {}
