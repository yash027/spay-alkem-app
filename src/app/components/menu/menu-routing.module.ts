import { authGuard } from "./../../guard/auth.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MenuPage } from "./menu.page";

const routes: Routes = [
  {
    path: "",
    component: MenuPage,
    children: [
      {
        path: "inbox",
        loadChildren: () =>
          import("./inbox/inbox.module").then(m => m.InboxPageModule)
      },
      {
        path: "request",
        loadChildren: () =>
          import("./request/request.module").then(m => m.RequestPageModule)
      },
      {
        path: "report",
        loadChildren: () =>
          import("./report/report.module").then(m => m.ReportPageModule)
      },
      {
        path: "",
        redirectTo: "/menu/inbox",
        pathMatch: "full"
      },
      {
        path: "**",
        redirectTo: "menu/inbox",
        pathMatch: "full"
      }
    ],
    canActivate: [authGuard]
  },
  {
    path: "",
    redirectTo: "/menu/inbox",
    pathMatch: "full",
    canActivate: [authGuard]
  },
  {
    path: "**",
    redirectTo: "/menu/inbox",
    pathMatch: "full",
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuPageRoutingModule {}
