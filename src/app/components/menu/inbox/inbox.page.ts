import { PopoverController, Platform } from "@ionic/angular";
import { ProfilePage } from "./../../profile-popover/profile.page";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GlobalService } from "../../../services/global.service";

@Component({
  selector: "app-inbox",
  templateUrl: "./inbox.page.html",
  styleUrls: ["./inbox.page.scss"]
})
export class InboxPage implements OnInit {
  dataInbox: any;
  loading = false;
  backbutton = 0;
  subscribe;
  constructor(
    private popoverController: PopoverController,
    private activatedroute: ActivatedRoute,
    public global: GlobalService,
    private router: Router,
    private platform: Platform
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.initializeApp();
    this.dataInbox = this.activatedroute.snapshot.data["inboxData"];
  }

  async profilePopover(ev) {
    const popover = await this.popoverController.create({
      component: ProfilePage,
      event: ev
    });
    return await popover.present();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.subscribe = this.platform.backButton.subscribeWithPriority(
        666666,
        () => {
          if (this.router.url == "/menu/inbox") {
            if (this.backbutton == 0) {
              this.backbutton++;
              this.global.displayToast("press again to exit.");
              setTimeout(() => {
                this.backbutton = 0;
              }, 3000);
            } else navigator["app"].exitApp();
          }
        }
      );
      //this.statusBar.styleDefault();
    });
  }
}
