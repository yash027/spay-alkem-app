import { authGuard } from "./../../../guard/auth.guard";
import { MenuInboxService } from "./../../../services/menu-services/menu-inbox.service";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { InboxPage } from "./inbox.page";

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        component: InboxPage,
        resolve: { inboxData: MenuInboxService }
      },
      {
        path: ":id",
        loadChildren: () =>
          import("./request-list/request-list.module").then(
            m => m.RequestListPageModule
          )
      },
      {
        path: "**",
        redirectTo: "menu/inbox",
        pathMatch: "full"
      }
    ],
    canActivate: [authGuard]
  },
  {
    path: "",
    redirectTo: "menu/inbox",
    pathMatch: "full",
    canActivate: [authGuard]
  },
  {
    path: "**",
    redirectTo: "menu/inbox",
    pathMatch: "full",
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InboxPageRoutingModule {}
