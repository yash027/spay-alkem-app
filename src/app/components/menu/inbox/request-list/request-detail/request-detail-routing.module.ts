import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { RequestDetailPage } from "./request-detail.page";
import { authGuard } from "src/app/guard/auth.guard";

const routes: Routes = [
  {
    path: "",
    component: RequestDetailPage,
    canActivate: [authGuard]
  },
  {
    path: "list-data-on-search-order",
    loadChildren: () =>
      import("./list-data-on-search/list-data-on-search.module").then(
        m => m.ListDataOnSearchPageModule
      ),
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestDetailPageRoutingModule {}
