import { MaterialModule } from "./../../../../material.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { RequestDetailPageRoutingModule } from "./request-detail-routing.module";

import { RequestDetailPage } from "./request-detail.page";
import { ListDataOnSearchPageModule } from "./list-data-on-search/list-data-on-search.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestDetailPageRoutingModule,
    MaterialModule,
    ListDataOnSearchPageModule
  ],
  declarations: [RequestDetailPage]
})
export class RequestDetailPageModule {}
