import { InboxApproveService } from "./../../../../../services/menu-services/submenu-services/Inbox-approve.service";
import { ListDataOnSearchPage } from "./list-data-on-search/list-data-on-search.page";
import { GlobalService } from "../../../../../services/global.service";
import { Component } from "@angular/core";
import {
  NavParams,
  ModalController,
  ToastController,
  AlertController
} from "@ionic/angular";

@Component({
  selector: "app-request-detail",
  templateUrl: "./request-detail.page.html",
  styleUrls: ["./request-detail.page.scss"]
})
export class RequestDetailPage {
  segmentChange = "info";
  requestData: any;
  returnedDataOrder = "";
  returnedDataCost = "";
  returnedDataGL = "";
  returnedDataFarea = "";
  loading = false;

  constructor(
    private navParams: NavParams,
    private modalCtrl: ModalController,
    public dateService: GlobalService,
    private toastController: ToastController,
    private service: InboxApproveService,
    private alertController: AlertController,
    private globalService: GlobalService,
  ) {}

  ionViewWillEnter() {
    this.requestData = this.navParams.get("data");
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

  segmentChanged(event) {
    this.segmentChange = event.detail.value;
  }

  toggleSagment() {
    let primaryFile = this.requestData.attachments.find(item => {
      if (item.primaryDocumnet === "X") {
        return item;
      }
    });
    if (!primaryFile) {
      this.toastController
        .create({
          message: "No Primary Document found",
          duration: 2000,
          position: "middle"
        })
        .then(toast => {
          toast.present();
        });
    } else {
      const link = document.createElement("a");
      link.setAttribute("target", "_blank");
      link.setAttribute("href", primaryFile.url);
      link.setAttribute("download", primaryFile.fileName);
      document.body.appendChild(link);
      link.click();
      link.remove();
    }
  }

  downloadFile(url, name) {
    const link = document.createElement("a");
    link.setAttribute("target", "_blank");
    link.setAttribute("href", url);
    link.setAttribute("download", name);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  openlistPopover(dataKey, dataDescription) {
    this.modalCtrl
      .create({
        component: ListDataOnSearchPage,
        componentProps: {
          datakey: dataKey,
          dataDesc: dataDescription
        }
      })
      .then(modal => {
        modal.present();
        modal.onWillDismiss().then(returnedRow => {
          if (returnedRow.data.row) {
            // console.log(returnedRow.data.key);
            if (
              returnedRow.data.key == "AUFNR" ||
              returnedRow.data.key == "KTEXT"
            ) {
              this.returnedDataOrder = returnedRow.data.row.name;
            } else {
              if (
                returnedRow.data.key == "KOSTL" ||
                returnedRow.data.key == "LTEXT"
              ) {
                this.returnedDataCost = returnedRow.data.row.name;
              } else {
                if (
                  returnedRow.data.key == "SAKNR" ||
                  returnedRow.data.key == "TXT20"
                ) {
                  this.returnedDataGL = returnedRow.data.row.name;
                } else {
                  if (
                    returnedRow.data.key == "FKBER" ||
                    returnedRow.data.key == "FKBTX"
                  ) {
                    this.returnedDataFarea = returnedRow.data.row.name;
                  } else {
                    // this.returnedDataOrder = "";
                    // this.returnedDataCost = "";
                    // this.returnedDataGL = "";
                    // this.returnedDataFarea = "";
                    this.globalService.displayToast("No Data Found", "dark");
                  }
                }
              }
            }
          }
        });
      });
  }

  submitRequest(buttonType) {
    const form = new FormData();
    let apiUrl;
    apiUrl =
      "/requests/inbox/requestType/" +
      this.requestData.requestType +
      "/requests/" +
      this.requestData.requestId +
      "/submit";
    this.alertController
      .create({
        header: "Approve !",
        backdropDismiss: false,
        inputs: [
          {
            name: "Comment",
            type: "text",
            label: "Comments",
            placeholder: "Add comments*"
          }
        ],
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            cssClass: "secondary",
            handler: () => {
              this.globalService.displayToast("Submission cancelled");
            }
          },
          {
            text: "Ok",
            handler: data => {
              this.loading = true;
              if (data.Comment) {
                const uploaddata = {
                  requestData: JSON.stringify({
                    header: this.requestData.header,
                    lineItems: this.requestData.lineItems,
                    activities:
                      buttonType == "Approve"
                        ? [this.requestData.activities[0]]
                        : buttonType == "Reject"
                        ? [this.requestData.activities[1]]
                        : [],
                    comment: data.Comment
                  })
                };
                for (let prop in uploaddata) {
                  form.append(prop, uploaddata[prop]);
                }
                if (this.requestData.attachments.length > 0) {
                  this.requestData.attachments.forEach(item => {
                    form.append("files", item);
                  });
                }
                this.service.approveRequest(apiUrl, form).subscribe(
                  response => {
                    this.toastController
                      .create({
                        showCloseButton: true,
                        message:
                          buttonType == "Approve"
                            ? "Request approved successfully"
                            : buttonType == "Reject"
                            ? "Request Rejected successfully"
                            : "",
                        duration: 3000,
                        position: "middle",
                        color: "success"
                      })
                      .then(toast => {
                        toast.present();
                      });
                    this.loading = false;
                    this.modalCtrl.dismiss();
                  },
                  error => {
                    this.loading = false;
                    this.globalService.displayToast("Error Occured", "danger");
                  }
                );
              } else {
                this.loading = false;
                this.globalService.displayToast(
                  "add comments and try again.",
                  "danger"
                );
              }
            }
          }
        ]
      })
      .then(alert => {
        alert.present();
      });
  }
}
