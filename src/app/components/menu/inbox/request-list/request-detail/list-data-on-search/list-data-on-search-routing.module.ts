import { authGuard } from "./../../../../../../guard/auth.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ListDataOnSearchPage } from "./list-data-on-search.page";

const routes: Routes = [
  {
    path: "",
    component: ListDataOnSearchPage,
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListDataOnSearchPageRoutingModule {}
