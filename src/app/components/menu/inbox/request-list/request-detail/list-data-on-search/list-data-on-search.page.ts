import { GlobalService } from "./../../../../../../services/global.service";
import { ModalController, NavParams } from "@ionic/angular";
import { SearchOrderNumberService } from "../../../../../../services/menu-services/submenu-services/list-data-on-search/search-order-number.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";

export interface PeriodicElement {
  name: string;
  position: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  // { position: 1, name: "Hydrogen" },
  // { position: 2, name: "Helium" },
  // { position: 3, name: "Lithium" },
  // { position: 4, name: "Beryllium" },
  // { position: 5, name: "Boron" },
  // { position: 6, name: "Carbon" },
  // { position: 7, name: "Nitrogen" },
  // { position: 8, name: "Oxygen" },
  // { position: 9, name: "Fluorine" },
  // { position: 10, name: "Neon" }
];

@Component({
  selector: "app-list-data-on-search",
  templateUrl: "./list-data-on-search.page.html",
  styleUrls: ["./list-data-on-search.page.scss"]
})
export class ListDataOnSearchPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  data: any = {
    key: "",
    value: null
  };
  options: any = {};
  displayedColumns: string[] = ["Order no.", "Description"];
  dataSource: any = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  optionValue: string;

  constructor(
    private service: SearchOrderNumberService,
    private modalCtrl: ModalController,
    private navParam: NavParams,
    private globalService: GlobalService
  ) {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {}
  ionViewWillEnter() {
    this.data.key = this.navParam.get("datakey");
    this.options.first = this.navParam.get("datakey");
    this.options.second = this.navParam.get("dataDesc");
    console.log(this.options);
    if (this.options.first == "AUFNR") {
      this.optionValue = "Order no.";
    } else {
      if (this.options.first == "KOSTL") {
        this.optionValue = "Cost Center";
      } else {
        if (this.options.first == "SAKNR") {
          this.optionValue = "G/L Account";
        } else {
          if (this.options.first == "FKBER") {
            this.optionValue = "Functional Area";
          }
        }
      }
    }
    this.dataSource.paginator = this.paginator;
  }
  searchList() {
    if (this.data.key == "AUFNR" || "KTEXT") {
      this.service
        .getList(
          "/order_number/" +
            this.data.key +
            "/" +
            this.data.value +
            "/getlistdatabysearch?page=0&size=100",
          this.data
        )
        .subscribe(response => {
          this.dataSource = response;
        });
    } else if (this.data.key == "KOSTL" || "LTEXT") {
      this.service
        .getList(
          "/cost_center/" +
            this.data.key +
            "/" +
            this.data.value +
            "/getlistdatabysearch?page=0&size=100",
          this.data
        )
        .subscribe(response => {
          this.dataSource = response;
        });
    } else if (this.data.key == "SAKNR" || "TXT20") {
      this.service
        .getList(
          "/gl_account/" +
            this.data.key +
            "/" +
            this.data.value +
            "/getlistdatabysearch?page=0&size=100",
          this.data
        )
        .subscribe(response => {
          this.dataSource = response;
        });
    } else if (this.data.key == "FKBER" || "FKBTX") {
      this.service
        .getList(
          "/functional_area/" +
            this.data.key +
            "/" +
            this.data.value +
            "/getlistdatabysearch?page=0&size=100",
          this.data
        )
        .subscribe(response => {
          this.dataSource = response;
        });
    } else
      this.globalService.displayToast(
        "Error Occured while fetching list",
        "danger"
      );
  }
  closeModal() {
    this.modalCtrl.dismiss({ key: this.data.key });
  }
  onRowSelected(row) {
    this.modalCtrl.dismiss({
      row: row,
      key: this.data.key
    });
  }
}
