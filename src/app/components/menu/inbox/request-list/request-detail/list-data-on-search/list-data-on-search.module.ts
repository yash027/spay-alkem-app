import { MaterialModule } from '../../../../../material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListDataOnSearchPageRoutingModule } from './list-data-on-search-routing.module';

import { ListDataOnSearchPage } from './list-data-on-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListDataOnSearchPageRoutingModule,
    MaterialModule
  ],
  declarations: [ListDataOnSearchPage]
})
export class ListDataOnSearchPageModule {}
