import { InboxListService } from "./../../../../services/menu-services/submenu-services/inbox-list.service";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { RequestListPage } from "./request-list.page";
import { authGuard } from "src/app/guard/auth.guard";

const routes: Routes = [
  {
    path: "",
    component: RequestListPage,
    resolve: { listData: InboxListService },
    canActivate: [authGuard]
  },
  {
    path: "**",
    redirectTo: "menu/inbox",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestListPageRoutingModule {}
