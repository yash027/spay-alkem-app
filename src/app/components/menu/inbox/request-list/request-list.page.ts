import { GlobalService } from "./../../../../services/global.service";
import { RequestDetailPage } from "./request-detail/request-detail.page";
import { Component, OnInit } from "@angular/core";
import { ModalController, LoadingController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { RequestDetailService } from "src/app/services/menu-services/submenu-services/request-detail.service";

@Component({
  selector: "app-request-list",
  templateUrl: "./request-list.page.html",
  styleUrls: ["./request-list.page.scss"]
})
export class RequestListPage implements OnInit {
  requestData: any;
  constructor(
    private requestService: RequestDetailService,
    private modalCtrl: ModalController,
    private route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private service: GlobalService
  ) {}

  listData: any;
  ngOnInit() {}
  ionViewWillEnter() {
    this.listData = this.route.snapshot.data["listData"];
  }

  openModel(id) {
    this.loadingCtrl
      .create({
        message: "Please Wait...",
        showBackdrop: true
      })
      .then(loader => {
        loader.present();
      });
    this.requestService.getRequestDetail(id).subscribe(
      response => {
        this.requestData = response;
        this.callModal();
      },
      error => {
        this.loadingCtrl.dismiss();
        this.service.displayToast("Error while fetching requests.", "danger");
      }
    );
  }

  async callModal() {
    const modal = await this.modalCtrl.create({
      component: RequestDetailPage,
      componentProps: { data: this.requestData }
    });
    await modal.present();
    this.loadingCtrl.dismiss();
  }
}
