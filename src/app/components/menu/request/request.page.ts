import { GlobalService } from "../../../services/global.service";
import { PopoverController, Platform } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";
import { ProfilePage } from "../../profile-popover/profile.page";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-request",
  templateUrl: "./request.page.html",
  styleUrls: ["./request.page.scss"]
})
export class RequestPage implements OnInit {
  loading = false;
  backbutton = 0;
  subscribe;

  constructor(
    private popoverController: PopoverController,
    private route: ActivatedRoute,
    public global: GlobalService,
    private platform: Platform,
    private router: Router
  ) {}

  requestData: any;
  ngOnInit() {}
  ionViewWillEnter() {
    this.initializeApp();
    this.requestData = this.route.snapshot.data["requestData"];
    this.loading = false;
  }
  async profilePopover(event) {
    const popover = await this.popoverController.create({
      component: ProfilePage,
      event: event
    });
    return await popover.present();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.subscribe = this.platform.backButton.subscribeWithPriority(
        666666,
        () => {
          if (this.router.url == "/menu/request") {
            if (this.backbutton == 0) {
              this.backbutton++;
              this.global.displayToast("press again to exit.");
              setTimeout(() => {
                this.backbutton = 0;
              }, 3000);
            } else navigator["app"].exitApp();
          }
        }
      );
      //this.statusBar.styleDefault();
    });
  }
}
