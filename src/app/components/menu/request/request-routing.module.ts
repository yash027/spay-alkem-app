import { authGuard } from "./../../../guard/auth.guard";
import { MenuRequestService } from "./../../../services/menu-services/menu-request.service";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { RequestPage } from "./request.page";

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        component: RequestPage,
        resolve: { requestData: MenuRequestService }
      },
      {
        path: ":id",
        loadChildren: () =>
          import(
            "../request/request-initiation/request-initiation.module"
          ).then(m => m.RequestInitiationPageModule)
      },
      {
        path: "**",
        redirectTo: "menu/request",
        pathMatch: "full"
      }
    ],
    canActivate: [authGuard]
  },
  {
    path: "**",
    redirectTo: "menu/request",
    pathMatch: "full",
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestPageRoutingModule {}
