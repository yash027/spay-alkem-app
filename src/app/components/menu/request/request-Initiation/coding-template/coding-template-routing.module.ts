import { authGuard } from "./../../../../../guard/auth.guard";
import { CodingTemplateService } from "./../../../../../services/menu-services/submenu-services/coding-template.service";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CodingTemplatePage } from "./coding-template.page";

const routes: Routes = [
  {
    path: "",
    component: CodingTemplatePage,
    resolve: { templateData: CodingTemplateService },
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CodingTemplatePageRoutingModule {}
