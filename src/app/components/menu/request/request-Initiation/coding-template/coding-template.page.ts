import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-coding-template",
  templateUrl: "./coding-template.page.html",
  styleUrls: ["./coding-template.page.scss"]
})
export class CodingTemplatePage implements OnInit {
  data: any;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.data = this.route.snapshot.data["templateData"];
  }
}
