import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CodingTemplatePageRoutingModule } from './coding-template-routing.module';

import { CodingTemplatePage } from './coding-template.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CodingTemplatePageRoutingModule
  ],
  declarations: [CodingTemplatePage]
})
export class CodingTemplatePageModule {}
