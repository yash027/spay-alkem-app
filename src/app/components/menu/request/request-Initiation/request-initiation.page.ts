import { FormInitiateService } from "./../../../../services/menu-services/submenu-services/form-initiate.service";
import { MaterialModule } from "src/app/components/material.module";
import { ActivatedRoute, Router } from "@angular/router";
import { FormService } from "./../../../../services/menu-services/submenu-services/form.service";
import { CodingTemplatePage } from "./coding-template/coding-template.page";
import { GlobalService } from "../../../../services/global.service";
import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  NgModule,
  Compiler
} from "@angular/core";
import {
  PopoverController,
  IonicModule,
  Platform,
  AlertController,
  LoadingController
} from "@ionic/angular";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import {
  Plugins,
  CameraResultType,
  CameraSource,
  Capacitor
} from "@capacitor/core";
import { DomSanitizer, BrowserModule } from "@angular/platform-browser";
import { FormsModule, NgForm } from "@angular/forms";
import { CommonModule } from "@angular/common";

@Component({
  selector: "app-request-initiation",
  templateUrl: "./request-initiation.page.html",
  styleUrls: ["./request-initiation.page.scss"]
})
export class RequestInitiationPage implements OnInit {
  @ViewChild("dynamicForm", { read: ViewContainerRef, static: false })
  dynamicForm: ViewContainerRef;
  @ViewChild("dynamicLineItem", { read: ViewContainerRef, static: false })
  dynamicLineItem: ViewContainerRef;

  id: string;
  loading = false;
  files: any = [];
  primaryDocument: any;
  lineItem: any = [{}];
  header: any = {};
  lineItemContent: any = {};
  images: any = [];
  formdataHeader: any = {};
  formdataLineItem: any = {};
  subscribe: any;
  constructor(
    public globalService: GlobalService,
    private popoverCtrl: PopoverController,
    private service: FormService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private compiler: Compiler,
    private router: Router,
    private formservice: FormInitiateService,
    private platform: Platform,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public domSanitizer: DomSanitizer
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.loading = true;
    this.id = this.route.snapshot.params["id"];
    this.initializeApp();
    if (this.id === "PAYABLES_PO") {
      this.service.formData("/forms/form_mobile_po").subscribe((res: any) => {
        res.inboxHeaderContent = res.inboxHeaderContent.replace(/&quot;/g, '"');
        res.inboxLineItemContent = res.inboxLineItemContent.replace(
          /&quot;/g,
          '"'
        );
        this.formdataHeader = this.sanitizer.bypassSecurityTrustHtml(
          res.inboxHeaderContent
        );
        this.formdataLineItem = this.sanitizer.bypassSecurityTrustHtml(
          res.inboxLineItemContent
        );
        this.addComponent();
        this.addComponentLineItem();
        this.loading = false;
      });
    } else {
      this.loading = true;
      if (this.id === "PAYABLES_NPO") {
        this.service
          .formData("/forms/form_mobile_npo")
          .subscribe((res: any) => {
            res.inboxHeaderContent = res.inboxHeaderContent.replace(
              /&quot;/g,
              '"'
            );
            res.inboxLineItemContent = res.inboxLineItemContent.replace(
              /&quot;/g,
              '"'
            );
            this.formdataHeader = this.sanitizer.bypassSecurityTrustHtml(
              res.inboxHeaderContent
            );
            this.formdataLineItem = this.sanitizer.bypassSecurityTrustHtml(
              res.inboxLineItemContent
            );
            this.addComponent();
            this.addComponentLineItem();
            this.loading = false;
          });
      } else {
        this.loading = false;
        this.router.navigate(["/menu/request"]);
        this.globalService.displayToast("Not Available", "dark");
      }
    }
  }

  private addComponent() {
    let template = this.formdataHeader.changingThisBreaksApplicationSecurity;

    @Component({
      template: template
    })
    class DynamicComponent {
      constructor(public _parent: RequestInitiationPage) {}
    }

    @NgModule({
      imports: [FormsModule, BrowserModule, CommonModule, IonicModule],
      declarations: [DynamicComponent]
    })
    class DynamicComponentModule {}

    const mod = this.compiler.compileModuleAndAllComponentsSync(
      DynamicComponentModule
    );
    const factory = mod.componentFactories.find(
      comp => comp.componentType === DynamicComponent
    );
    const component = this.dynamicForm.createComponent(factory);
  }

  private addComponentLineItem() {
    let template = this.formdataLineItem.changingThisBreaksApplicationSecurity;

    @Component({
      template: template
    })
    class DynamicComponent {
      constructor(public _parent: RequestInitiationPage) {}
    }

    @NgModule({
      imports: [
        FormsModule,
        BrowserModule,
        CommonModule,
        IonicModule,
        MaterialModule
      ],
      declarations: [DynamicComponent]
    })
    class DynamicComponentModule {}

    const mod = this.compiler.compileModuleAndAllComponentsSync(
      DynamicComponentModule
    );
    const factory = mod.componentFactories.find(
      comp => comp.componentType === DynamicComponent
    );
    const component = this.dynamicLineItem.createComponent(factory);
  }

  onItemChange(e) {
    this.primaryDocument = e.value;
  }

  async codingTemplate() {
    const pop = await this.popoverCtrl.create({
      component: CodingTemplatePage
    });
    return await pop.present();
  }

  onClickAddItem() {
    this.lineItem.push({});
  }
  removeItem(index) {
    if (this.lineItem.length > 1) {
      this.lineItem.splice(index, 1);
    }
  }

  showFile(files: FileList) {
    if (files && files.length > 0) {
      for (let key in files) {
        if (key !== "length" && key !== "item") {
          const file = files[key];
          this.files.push(file);
          this.primaryDocument = file;
        }
      }
    }
  }

  onRemoveImage(index) {
    this.files.splice(index, 1);
  }
  openCamera() {
    if (!Capacitor.isPluginAvailable("Camera")) {
      this.globalService.displayToast("Unable To Open Camera", "dark");
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 60,
      source: CameraSource.Camera,
      height: 400,
      width: 300,
      correctOrientation: true,
      resultType: CameraResultType.DataUrl
    })
      .then(image => {
        const blob = this.globalService.dataURItoBlob(image.dataUrl);
        const img = {
          name: "Image-" + new Date().toISOString(),
          image: blob
        };
        this.files.push(img);
        // console.log(img);
        // console.log(this.files);
      })
      .catch(error => {
        return false;
      });
  }

  submitForm(comment) {
    this.loadingCtrl
      .create({
        message: "Please wait...",
        animated: true,
        translucent: true
      })
      .then(loader => {
        loader.present();
      });
    const data: any = [];
    let filedata: any = [];

    for (let i = 0; i < this.lineItem.length; i++) {
      data.push({
        sno: i + 1,
        lineItems: this.lineItem[i],
        subLineItems: {}
      });
    }
    for (let i = 0; i < this.files.length; i++) {
      if (this.files.length > 1) {
        if (this.files[i].name != this.primaryDocument.name) {
          filedata.push(this.files[i]);
        }
      } else filedata = [];
    }
    // console.log(this.files);
    // console.log(filedata);
    // console.log(this.primaryDocument);
    const uploaddata = {
      requestData: JSON.stringify({
        docType: this.id,
        description: comment.comment,
        header: this.header,
        lineItems: data
      }),
      primaryDocument: this.primaryDocument
    };
    const formData = new FormData();

    for (let key in uploaddata) {
      if (uploaddata[key]) {
        formData.append(key, uploaddata[key]);
      }
    }
    if (filedata.length > 0) {
      filedata.forEach(item => {
        formData.append("files", item);
      });
    }
    this.formservice.submitForm("/requests", formData).subscribe(
      response => {
        this.loadingCtrl.dismiss();
        this.alertCtrl
          .create({
            header: "Successfull",
            message: `Request ${response["requestId"]} was generated successfully.`,
            backdropDismiss: false,
            buttons: [
              {
                text: "Done",
                role: "cancel",
                handler: () => {}
              }
            ]
          })
          .then(alert => {
            alert.present();
          });
        this.router.navigate(["/menu/request"]);
      },
      error => {
        this.globalService.displayToast(
          "Server not responding, Please try later.",
          "danger"
        );
        this.router.navigate(["menu/request"]);
      }
    );
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.subscribe = this.platform.backButton.subscribeWithPriority(
        666666,
        () => {
          this.closeForm();
          this.statusBar.styleDefault();
        }
      );
    });
  }
  closeForm() {
    if (
      this.router.url == "/menu/request/PAYABLES_NPO" ||
      this.router.url == "/menu/request/PAYABLES_PO"
    ) {
      this.alertCtrl
        .create({
          header: "Request Initiation",
          message: "Going Back Will DELETE the data.",
          buttons: [
            {
              text: "Cancel",
              role: "cancel",
              cssClass: "secondary",
              handler: blah => {
                console.log("Confirm Cancel: blah");
              }
            },
            {
              text: "Okay",
              cssClass: "danger",
              handler: () => {
                this.router.navigate(["/menu/request"]);
                // location.reload();
              }
            }
          ]
        })
        .then(alert => {
          alert.present();
        });
    }
  }
}
