import { MaterialModule } from 'src/app/components/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestInitiationPageRoutingModule } from './request-initiation-routing.module';

import { RequestInitiationPage } from './request-initiation.page';
import { CodingTemplatePageModule } from './coding-template/coding-template.module';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestInitiationPageRoutingModule,
    CodingTemplatePageModule,
    MaterialModule
  ],
  declarations: [RequestInitiationPage]
})
export class RequestInitiationPageModule {}
