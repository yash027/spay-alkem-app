import { authGuard } from "./../../../../guard/auth.guard";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { RequestInitiationPage } from "./request-initiation.page";

const routes: Routes = [
  {
    path: "",
    component: RequestInitiationPage,
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestInitiationPageRoutingModule {}
