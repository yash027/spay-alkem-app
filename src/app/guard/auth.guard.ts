import { authService } from "src/app/services/auth/auth.service";
import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";


export class authGuard implements CanActivate {
  constructor(private service: authService, private router: Router) {}

  canActivate() {
    if (this.service.isAuthorized()) {
      return true;
    } else {
      this.router.navigate(["login"]);
      return false;
    }
  }
}
