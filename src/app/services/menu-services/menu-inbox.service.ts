import { HttpService } from "../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MenuInboxService  {
  constructor(private service: HttpService) {}

  resolve() {
    return this.service.GET("/requests/inbox");
  }
}
