import { HttpService } from "./../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MenuRequestService {
  constructor(private service: HttpService) {}

  resolve() {
    return this.service.GET("/request-types");
  }
}
