import { HttpService } from "./../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MenuReportService {
  constructor(private service: HttpService) {}
  resolve() {
    return this.service.GET(
      "/requests/initiatereports?page=0&size=10&flowType=SAP"
    );
  }
}
