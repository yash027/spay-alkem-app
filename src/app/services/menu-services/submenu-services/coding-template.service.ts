import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CodingTemplateService {
  constructor(private service: HttpService) {}

  resolve() {
    return this.service.GET("/coding-templates/reqType/PAYABLES_NPO");
  }
}
