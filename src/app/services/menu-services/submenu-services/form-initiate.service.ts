import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class FormInitiateService {
  constructor(private http: HttpService) {}

  submitForm(url, data) {
    return this.http.POST(url, data);
  }
}
