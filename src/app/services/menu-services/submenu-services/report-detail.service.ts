import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ReportDetailService {
  constructor(private service: HttpService) {}

  resolve() {
    return this.service.GET("/requests/initiatereports/");
  }
}
