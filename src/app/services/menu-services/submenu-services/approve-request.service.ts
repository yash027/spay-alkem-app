import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ApproveRequestService {
  constructor(private http: HttpService) {}

  sendApproveRequest(url, data) {
    return this.http.POST(url, data);
  }
}
