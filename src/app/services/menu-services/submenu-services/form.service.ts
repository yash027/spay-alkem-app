import { HttpService } from "../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class FormService {
  constructor(private service: HttpService) {}

  formData(url) {
    return this.service.GET(url);
  }
}
