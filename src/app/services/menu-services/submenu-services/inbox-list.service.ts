import { ActivatedRouteSnapshot } from "@angular/router";
import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class InboxListService {
  requestType = new BehaviorSubject("");
  constructor(private service: HttpService) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.requestType.next(route.params.id);
    return this.service.GET("/requests/inbox/requestType/" + route.params.id);
  }
}
