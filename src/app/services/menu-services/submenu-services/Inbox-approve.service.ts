import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class InboxApproveService {
  constructor(private http: HttpService) {}

  approveRequest(url,data) {
    return this.http.POST(url, data);
  }
}
