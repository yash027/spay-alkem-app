import { HttpService } from './../../../http/http.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchOrderNumberService {

  constructor(private http: HttpService) { }

  getList(url,data) {
    return this.http.POST(url,data);
  }
}
