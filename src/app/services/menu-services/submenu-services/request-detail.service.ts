import { InboxListService } from "./inbox-list.service";
import { HttpService } from "./../../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class RequestDetailService {
  requestType: string;

  constructor(
    private service: HttpService,
    private listservice: InboxListService
  ) {}

  getRequestDetail(id) {
    this.listservice.requestType.subscribe(data => {
      this.requestType = data;
    });
    return this.service.GET(
      "/requests/inbox/requestType/" + this.requestType + "/requests/" + id
    );
  }
}
