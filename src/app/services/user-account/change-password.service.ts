import { HttpService } from "./../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ChangePasswordService {
  constructor(private http: HttpService) {}

  changePassword(url, data) {
    return this.http.POST(url, data);
  }
}
