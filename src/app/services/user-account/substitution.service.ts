import { HttpService } from "./../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class SubstitutionService {
  constructor(private service: HttpService) {}

  substituteList(url) {
    return this.service.GET(url);
  }
}
