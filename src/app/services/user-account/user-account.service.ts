import { HttpService } from './../http/http.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserAccountService {

  constructor(private http: HttpService) { }

  accountInfo(url) {
    return this.http.GET(url);
  }
}
