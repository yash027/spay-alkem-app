import { environment } from "./../../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class HttpService {
  baseUrl = environment.url;
  constructor(private service: HttpClient) {}

  GET(url) {
    return this.service.get(this.baseUrl + url);
  }
  POST(url, data) {
    return this.service.post(this.baseUrl + url, data);
  }
  PUT(url, data) {
    return this.service.put(this.baseUrl + url, data);
  }
}
