import { Injector } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from "@angular/common/http";
import { authService } from "../auth/auth.service";

export class AuthIntercepterService implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let token: string;
    let Authservice = this.injector.get(authService);
    if (!Authservice.getToken()) {
      token = "";
    } else {
      token = Authservice.getToken();
    }
    let tokanizedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
    return next.handle(tokanizedReq);
  }
}
