import { ToastController } from "@ionic/angular";
import { Injectable } from "@angular/core";
import { Network } from "@ionic-native/network/ngx";

@Injectable({
  providedIn: "root"
})
export class networkService {
  constructor(private network: Network, private toast: ToastController) {
    //this.globalService.displayToast("Internet Connected", "success", 2000);
    this.network.onConnect().subscribe(connect => {
      console.log(connect);
      this.toast
        .create({
          message: "Internet Connected",
          color: "success",
          duration: 2000
        })
        .then(toast => {
          toast.present();
        });
    });
    this.network.onDisconnect().subscribe(disconnect => {
      this.toast
        .create({
          message: "Not Connected To Internet, Please Check Your Connection",
          color: "danger",
          duration: 2000
        })
        .then(toast => {
          toast.present();
        });
    });
  }
}
