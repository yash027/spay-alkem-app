import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class GlobalService {
  colors = ["tertiary", "secondary", "success"];
  constructor(private toastCtrl: ToastController) {}

  userData = new BehaviorSubject(null);
  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(",")[0].indexOf("base64") >= 0) {
      byteString = atob(dataURI.split(",")[1]);
    } else {
      byteString = unescape(dataURI.split(",")[1]);
    }
    // separate out the mime component
    let mimeString = dataURI
      .split(",")[0]
      .split(":")[1]
      .split(";")[0];

    // write the bytes of the string to a typed array
    let ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
  }

  bytesToSize(bytes, decimals = 2) {
    if (bytes === 0) return "0 Bytes";
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }
  getPortalDate(date) {
    if (date) {
      const year = date.slice(0, 4);
      const month = date.slice(4, 6);
      const day = date.slice(6, 8);
      const newDate = day + "/" + month + "/" + year;
      return newDate;
    }
  }
  getColorClass(index) {
    if (index >= this.colors.length) {
      while (index >= this.colors.length) {
        index = index - this.colors.length;
      }
    }
    return this.colors[index];
  }

  displayToast(msg, color = "dark", duration = 2000) {
    this.toastCtrl
      .create({
        message: msg,
        duration: duration,
        color: color || "dark",
        position: "bottom",
        showCloseButton: true
      })
      .then(toast => {
        toast.present();
      });
  }
}
