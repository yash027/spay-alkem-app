import { environment } from "./../../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class authService {
  constructor(private service: HttpClient) {}

  baseUrl = environment.url;
  loginUser(url, data) {
    const config = {
      headers: {
        "user-agent":
          "Mozilla/4.0 (compatible; MSIE 6.0; " +
          "Windows NT 5.2; .NET CLR 1.0.3705;)"
      }
    };
    return this.service.post(this.baseUrl + url, data, config);
  }

  isAuthorized() {
    return !!localStorage.getItem("token");
  }

  getToken() {
    return localStorage.getItem("token");
  }
}
