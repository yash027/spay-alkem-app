import { HttpService } from "./../http/http.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class forgetUserService {
  constructor(private service: HttpService) {}

  forgetUserPassword(url, email) {
    return this.service.POST(url, email);
  }
}
